# Erento Tech test

# Q1: Backend application:

## Requirements

On this task, you score more by explaining your design decisions than you do by providing actual implementations without context. Plaintext description is fine.

You have a list of 100k addresses with geo coordinates. Design a system with a function that will:

- filter out items that are more than 20km away from geo point sent as function parameter
- sort items left after filtering from nearest to farthest
- your system should be efficient in terms of cpu, memory, I/O usag
- you can pick a way to store the items in a database

## Arquitecture

I decided to do this exercise as a MondoDB + Node.js (Express) API microservice serving JSON objects.

A flat Node + Database microservice API has very little overhead and is very efficient on CPU and Memory. The Node.js + Express backend can scale to high numbers of connections per second. The bottleneck is on the Mongo, but Mongo itself is fast and can scale  horizontally as required. This Node.js + MongoDB arquitecture has no I/O hit.  

## Dataset

I decided to write a simplified microservice for the backend part. I needed a dataset with geolocalized data, and for that I used  Geonames places. You can find those here http://download.geonames.org/export/dump/ We don't need the whole database. I used only the German points, listing German cities and their coordinates. It has over >150K datapoints so it is a good example for this test.

The file has a structure like this

```
> tail DE.zip
2658739	Schiener Bach	Schiener Bach	Schiener Bach	47.6802	8.86131	H	STM	DE	DE,CH	00				0		512	Europe/Zurich	2015-09-06
(...)
```

For this exercise we are only interested on the place itself and its geolocation

```
> cut -f2,5-6 DE.txt > DE_town_lat_long.txt
> tail DE_town_lat_long.txt
Überlingen St. Nikolaus	47.76722	9.16056
Villinger Münster	48.0603	8.45887
Wehrshausen Marienkirche	50.81176	8.72428
Kloster Reuthin	48.62137	8.74562
Dominikanerkirche Bad Wimpfen	49.2285	9.16097
Bad Wimpfen Stiftskirche St. Peter	49.2317	9.17847
Evangelische Kirche Winnen	50.69551	8.83023
Dom St. Peter zu Worms	49.63028	8.35972
Aldekerk St. Peter und Paul	51.4397	6.41608
TC Schachenbad e.V.	47.9417	10.27209
```

# Database

For complex geospacial queries postgresql still has a clear advantage with its full GIS support. Other databases have geospacial support for simple searches. This means if we were looking for complex area searches with complex objects we will probable should use postgresql (or at least consult with a GIS expert) but for this exercise MariaSQL or Mongo should be fine and actually probably perform better.

For this test I decided to use mongodb because:
- I expect a real application will have a few orders of magnitude more datapoints so we want to use something that scale horizontally like mongo.
- It is really easy/cheap to host a mongodb database for a test like this with a third party service like mlab which simplifies a lot the dependencies needed to run the example

To import the dataset we need to massage a little bit the data to remove problematic characters and create a json like structure

```
cat DE_town_lat_long.txt | sed 's/[" | \\ | \/]//g' | sed 's:\(.*\)\t\(.*\)\t\(.*\):{place\:"\1",loc\:[\2,\3]}:' > DE_town_lat_long_as_loc.txt
```

And now we can import it on the mongodb 3.x

```
> mongoimport -h ds031965.mlab.com --port 31965 -d geonames_de -u erento -p b8FHug -c addresses --file DE_town_lat_long_as_loc.txt
2016-07-30T15:34:45.007+0200	connected to: ds031965.mlab.com:31965
2016-07-30T15:34:47.211+0200	[#.......................] geonames_de.addresses	389.0 KB/7.9 MB (4.8%)
2016-07-30T15:34:50.210+0200	[###.....................] geonames_de.addresses	1.0 MB/7.9 MB (12.7%)
2016-07-30T15:34:53.211+0200	[####....................] geonames_de.addresses	1.6 MB/7.9 MB (20.6%)
2016-07-30T15:34:56.210+0200	[######..................] geonames_de.addresses	2.2 MB/7.9 MB (28.1%)
2016-07-30T15:34:59.210+0200	[########................] geonames_de.addresses	2.9 MB/7.9 MB (36.1%)
2016-07-30T15:35:02.210+0200	[##########..............] geonames_de.addresses	3.4 MB/7.9 MB (43.4%)
2016-07-30T15:35:05.210+0200	[############............] geonames_de.addresses	4.1 MB/7.9 MB (51.3%)
2016-07-30T15:35:08.211+0200	[#############...........] geonames_de.addresses	4.5 MB/7.9 MB (56.6%)
2016-07-30T15:35:11.210+0200	[###############.........] geonames_de.addresses	5.1 MB/7.9 MB (64.6%)
2016-07-30T15:35:14.211+0200	[################........] geonames_de.addresses	5.5 MB/7.9 MB (68.8%)
2016-07-30T15:35:17.210+0200	[##################......] geonames_de.addresses	6.0 MB/7.9 MB (76.0%)
2016-07-30T15:35:20.210+0200	[###################.....] geonames_de.addresses	6.4 MB/7.9 MB (80.3%)
2016-07-30T15:35:23.210+0200	[#####################...] geonames_de.addresses	7.0 MB/7.9 MB (87.9%)
2016-07-30T15:35:26.211+0200	[#######################.] geonames_de.addresses	7.6 MB/7.9 MB (96.1%)
2016-07-30T15:35:27.653+0200	[########################] geonames_de.addresses	7.9 MB/7.9 MB (100.0%)
2016-07-30T15:35:27.654+0200	imported 186488 documents
```

We should have an index on this field
```
> db.addresses.createIndex({loc: "2d"})
```

And I do the first spacial query checking places around the area where I am at the moment. 

*$near already sorts the results by distance*. The spec doesn't say anything about the number of results, so I'm going to go with the default first 100 results.*

```
> db.addresses.find({loc: { $near: [49.7939,9.9512]}})
{ "_id" : ObjectId("579cacf682428bae43b0c125"), "place" : "Würzburg", "loc" : [ 49.79391, 9.95121 ] }
{ "_id" : ObjectId("579cad1e82428bae43b37abe"), "place" : "GhotelHotel&LivingWürzburg", "loc" : [ 49.79895, 9.9451 ] }
{ "_id" : ObjectId("579cacfa82428bae43b10d83"), "place" : "Südbahnhof", "loc" : [ 49.78806, 9.94306 ] }
{ "_id" : ObjectId("579cad1a82428bae43b3289e"), "place" : "DorintNovotel", "loc" : [ 49.7968, 9.9413 ] }
{ "_id" : ObjectId("579cad0f82428bae43b28a7f"), "place" : "Galgenberg", "loc" : [ 49.78333, 9.95 ] }
{ "_id" : ObjectId("579cad1a82428bae43b32817"), "place" : "TOPAmberger", "loc" : [ 49.79638, 9.93878 ] }
{ "_id" : ObjectId("579cad1f82428bae43b38517"), "place" : "Poppular", "loc" : [ 49.8, 9.94 ] }
{ "_id" : ObjectId("579cad1f82428bae43b3850c"), "place" : "ZurStadtMainz", "loc" : [ 49.8, 9.94 ] }
{ "_id" : ObjectId("579cad1f82428bae43b38513"), "place" : "HotelUndRestaurantGoldenesFass", "loc" : [ 49.8, 9.94 ] }
{ "_id" : ObjectId("579cad0082428bae43b17a8b"), "place" : "Pleichachbach", "loc" : [ 49.80088, 9.9381 ] }
{ "_id" : ObjectId("579cad1a82428bae43b32351"), "place" : "KreisfreieStadtWürzburg", "loc" : [ 49.7822, 9.94041 ] }
{ "_id" : ObjectId("579cad1e82428bae43b37681"), "place" : "Moto-Act.de", "loc" : [ 49.79523, 9.93422 ] }
{ "_id" : ObjectId("579cad1c82428bae43b35401"), "place" : "Würzburg", "loc" : [ 49.79708, 9.93365 ] }
{ "_id" : ObjectId("579cad2082428bae43b390c2"), "place" : "WürzburgerDom", "loc" : [ 49.79338, 9.9324 ] }
{ "_id" : ObjectId("579cad1e82428bae43b36905"), "place" : "Posthalle", "loc" : [ 49.80181, 9.93291 ] }
{ "_id" : ObjectId("579cad1e82428bae43b37a3f"), "place" : "BestWesternPremierHotelRebstock", "loc" : [ 49.79039, 9.93054 ] }
{ "_id" : ObjectId("579cad1b82428bae43b331b7"), "place" : "BestWesternPremierHtlRebst", "loc" : [ 49.7902, 9.9303 ] }
{ "_id" : ObjectId("579cad0e82428bae43b27726"), "place" : "Grainberg", "loc" : [ 49.80201, 9.97098 ] }
{ "_id" : ObjectId("579cad1f82428bae43b38516"), "place" : "TillEulenspiegelNichtrauchotel", "loc" : [ 49.79, 9.93 ] }
{ "_id" : ObjectId("579cad1f82428bae43b38520"), "place" : "AlterKranen", "loc" : [ 49.79, 9.93 ] }
```

We can also run the query with a maximum distance, returning all items on a circle of that distance as radios, centered on that point. This is already very close to the required spec.

```
> db.addresses.find({loc: { $near: [52.52003, 13.40489],$maxDistance: 1/6378.1}})
{ "_id" : ObjectId("579cad1b82428bae43b3329c"), "place" : "Mitte", "loc" : [ 52.52003, 13.40489 ] }
{ "_id" : ObjectId("579cad1f82428bae43b37cbc"), "place" : "WintersHotelBerlinMitteTheWall", "loc" : [ 52.52, 13.405 ] }
```

## Code

Screencast:

![BackendScreencast](/backendScreencast.gif "BackendScreencast")

**Find the code at backend/**

Super simple Node + Express microservice. Express is better than a plain Node application because then we can easily plug Auth and SSL modules.

Possible improvements:

- Authentication requests 
- The bottleneck is the API-database connection. As the locations are
  inmutable, it makes sense to implement a small
  cache to prevent hitting the database for repeated queries. 
  (I would just use a gateway cache server like varnish in front of the API)
- Add clustering, so there is a node process per machine CPU to increase the
  number of connections per second
- Remove console.logs and use instead a logging library
- Operations stuff: Docker image, make sure node restarts, run it on production
  environment etc.
- Better scaffolding so code maintenance is easier (configuration files, etc)

# Q2: Frontend application

## Requirements

- create a demo JS frontend application - a gallery of images
- you can pick your framework of choice (ideally angular.js, but for example ember.js, backbone.js would also work)
- consume the API from http://jsonplaceholder.typicode.com/
- the gallery would be using the resource at http://jsonplaceholder.typicode.com/albums/
- the idea is to show a list of albums, and clicking an album should show the photos in it
- to not waste your time, test automation is optional, and we are mostly interested in seeing how you structure your app and reading some comments about this in the source code

## Code

Screencast:

![FrontendScreencast](/frontendScreencast.gif "FrontendScreencast")

**Find the code at frontend/**


I used React for this test. The user can see a list of all available albums, and when one of them is selected, it opens the photos on it. If one of the photos is selected, it opens a full size image (In this case a square 600px wide picture). As packager I used npm+webpack. This is my standard tool these days. I'm learning about the new EC6 features and syntax so I used babel to transpile the code into the common EC5 code all browsers can run. For linting, I used ESLint using Airbnb rules. 

How would I improve this work:

- Better test coverage - Marked as optional on the requirements
- It still lacks important features an SLA should have like for example data caching and auth.
- Global (persisted) SLA state (using redux for example)
- Components should not be destroyed all the time, because the client is then
  forced to do frequent API requests. It would be nicer to present the big picture on a higher layer, like a modal window or a lightbox.
- Adding features: Upload an image, create a new album, display only user's
  album, sorting of pictures... there are so many things we could do, the possibilities are endless and all of them out of scope of the test.

