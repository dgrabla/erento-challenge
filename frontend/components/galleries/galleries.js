// DGB 2016-08-02 19:25
// Entry point for the component

import React from 'react';
import _ from 'lodash';

import Gallery from './components/gallery';
import GalleriesList from './components/galleriesList';

export default class Galleries extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      currentGallery: false,
      currentPhoto: false,
    };
    // DGB 2016-08-02 19:26 EC6 style doesn't bind by default
    this.onOpenGallery = this.onOpenGallery.bind(this);
    this.onOpenPhoto = this.onOpenPhoto.bind(this);
    this.onClosePhoto = this.onClosePhoto.bind(this);
    this.onCloseGallery = this.onCloseGallery.bind(this);
  }

  componentDidMount() {
    console.log('Loaded Galleries - main component');
  }

  onOpenGallery(index, event) {
    console.log('Selected Gallery: ' + index);
    this.setState({
      currentGallery: index,
    });
  }

  onOpenPhoto(index, event) {
    console.log('Selected Photo: ' + index);
    this.setState({
      currentPhoto: index,
    });
  }

  onClosePhoto(index, event) {
    this.setState({
      currentPhoto: false,
    });
  }

  onCloseGallery(index, event) {
    this.setState({
      currentGallery: false,
    });
  }

  render() {
    if (!this.state.currentPhoto && !this.state.currentGallery) {
      return (
        <GalleriesList onOpenGallery={this.onOpenGallery} data={this.props.data} />
      );
    } else if (this.state.currentPhoto) {
      return (
        <div>
          <div onClick={this.onClosePhoto} className="btn btn-default">← Go back to gallery</div>
          <img className="bigImage" alt="Big Selection" src={this.state.currentPhoto} />
        </div>
        );
    } else if (this.state.currentGallery) {
      return (
        <Gallery onCloseGallery={this.onCloseGallery} onOpenPhoto={this.onOpenPhoto} data={_.find(this.props.data, { id: this.state.currentGallery })} />
      );
    }
    return (
      <div>There was an error</div>
    );
  }
}

Galleries.propTypes = {
  data: React.PropTypes.object,
};
