import React from 'react';

export default class GalleriesList extends React.Component {

  render() {
    let galleryNodes = [];
    let gallery = {};
    let i = 0;
    for (i = 0; i < this.props.data.length; i++) {
      gallery = this.props.data[i];
      galleryNodes.push(
        <div key={gallery.id} data-id={gallery.id} onClick={this.props.onOpenGallery.bind(this, gallery.id)}>
          <div className="galleryTitle h3">{gallery.title}</div>
        </div>
      );
    }
    return (
      <div className="galleries">
        <div className="text-right">Erento Frontend Test</div>
        <h1>All available galleries</h1>
        {galleryNodes}
      </div>
    );
  }
}
GalleriesList.propTypes = {
  data: React.PropTypes.array,
  onOpenGallery: React.PropTypes.func,
};

