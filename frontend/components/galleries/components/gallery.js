import React from 'react';
import $ from 'jquery';

// DGB 2016-08-02 19:25
// This is a nasty dependency, it is probably better to have this as part
// of the configuration of the would be better to have this as part of the
// configuration of the component at galleries.js
import getApiEndpoint from '../../../utils/api';

export default class Gallery extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      data: false,
    };
  }

  componentDidMount() {
    $.ajax({
      url: getApiEndpoint('getGalleryPhotos', this.props.data.id),
      dataType: 'json',
      method: 'GET',
      cache: false,
      success: function (data) {
        this.setState({ data: data });
      }.bind(this),
      error: function (xhr, status, err) {
        console.error(this.url, status, err.toString());
      }.bind(this),
    });
  }

  render() {
    let photos = 'Loading all available pictures on this album...';
    if (this.state.data) {
      let i;
      let photo;
      photos = [];
      for (i = 0; i < this.state.data.length; i++) {
        photo = this.state.data[i];
        photos.push(
          <img alt="Thumbnail" className="thumbImage" key={photo.id} onClick={this.props.onOpenPhoto.bind(this, photo.url)} src={photo.thumbnailUrl} />
        );
      }
    }
    return (
      <div className="Gallery">
        <div onClick={this.props.onCloseGallery.bind(this)} className="btn btn-default">← Go back to all available Galleries</div>
        <h2 className="galleryTitle">
          {this.props.data.title}
        </h2>
       {photos}
      </div>
    );
  }
}

Gallery.propTypes = {
  data: React.PropTypes.object,
  onOpenPhoto: React.PropTypes.func,
  onCloseGallery: React.PropTypes.func,
};
