// DGB 2016-08-02 19:29
// I prefer to use a logger like winston, here I just use the console
// Remove of console for production
if (process.env.NODE_ENV !== 'production') {
  const console = {};
  console.log = function () {};
  console.error = function () {};
  console.info = function () {};
}

import $ from 'jquery';
import getApiEndpoint from './utils/api';
import React from 'react';
import ReactDOM from 'react-dom';
import Galleries from './components/galleries/galleries';

// DGB
// Just to prevent requesting this endpoint all the time
// on development
if (process.env.NODE_ENV === 'production') {
  $.ajax({
    url: getApiEndpoint('getAlbums'),
    dataType: 'json',
    method: 'GET',
    cache: false,
    success: function (data) {
      ReactDOM.render(
        <Galleries data={data} />,
        document.getElementById('main')
      );
    }.bind(this),
    error: function (xhr, status, err) {
      console.error(this.url, status, err.toString());
    }.bind(this),
  });
} else {
  console.info('DEBUG MODE - Using local album query');
  let localGalleriesRequest = require('./request_responses/galleries');
  ReactDOM.render(
    <Galleries data={localGalleriesRequest} />,
    document.getElementById('main')
  );
}
