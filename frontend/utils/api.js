export default function (name, id) {
  const apiHost = 'http://jsonplaceholder.typicode.com';
  const apiEndpoints = {
    getAlbums: function () {
      return '/albums';
    },
    getGalleryPhotos: function () {
      return '/albums/' + id + '/photos';
    },
  };
  return apiHost + apiEndpoints[name](id);
}

