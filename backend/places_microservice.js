// DGB 2016-07-30 17:04 
// Erento german places microservice API
// Install dependencies with 'npm install'
// Run with 'node places_microservice.js'

'use strict';

var express = require('express');
var mongodb = require('mongodb');
var _ = require('lodash');
var app = express();

var MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://erento:b8FHug@ds031965.mlab.com:31965/geonames_de', function(err, db) {
  if (err) {
    throw err;
  }
  console.log('\n✓ Service connected to Mongo\n')
  console.log('\n✓ Service is ready for client connections\n')

  app.get('/:lat/:lon', function (req, res) {
    console.log('Got a request lat:' + req.params.lat + ' lon:' + req.params.lon);
    var lat = _.toNumber(req.params.lat),
        lon = _.toNumber(req.params.lon),
        distance = 200/6378.1;

    if ((lat<-90 || lat>+90) || (lon<-180 || lon > +180)) {
      res.send(500,"Invalid Latitude or Longitude");
    }

    else {
      db.collection('addresses').find({loc: { $near: [lat, lon], $maxDistance: distance}}).toArray(function(err, result) {
        if (err) {
          throw err;
        }
        res.send(result);
      });
    }
  });

  app.get('/', function (req, res) {
    res.send("Welcome to Erento API German places geolocalization microservice. Hit me with /:latitude/:longitude. Try /52.52003/13.40489 for Berlin");
  })
});

app.listen(8080, function() {
  console.log('\n✓ Service started on port 8080.\n')
});
